#ifndef REGISTERS_PORT_HPP_
#define REGISTERS_PORT_HPP_

#include <cassert>

template<typename T>
struct Port {
	using ModerType = typename T::MODER::Type;

	static void Set(std::uint32_t value)
	{
		assert((value <= (1 << 16U)));
		T::BSRR::Write(static_cast<typename T::BSRR::Type>(value));
	}

	static void Reset(std::uint32_t value)
	{
		assert((value <= (1 << 16U)));
		T::BSRR::Write(static_cast<typename T::BSRR::Type>(value) << 16);
	}

	static void Toggle(std::uint32_t value)
	{
		assert((value <= (1 << 16U)));
		T::ODR::Toggle(static_cast<typename T::ODR::Type>(value));
	}

	static auto Get()
	{
		return T::IDR::Get();
	}

	static void SetAnalog(std::uint32_t pinNum)
	{
		assert(pinNum <= 15U);
		using RegType = T::MODER::Type;
		RegType::Set(
			T::MODER::Address,
			T::MODER::FieldValues::Analog::Mask,
			T::MODER::FieldValues::Analog::Value,
			static_cast<ModerType>(pinNum * uint8_t{2U}));
	}

	static void SetInput(std::uint32_t pinNum)
	{
		assert(pinNum <= 15U);
		ModerType::Set(
			T::MODER::Address,
			T::MODER::FieldValues::Input::Mask, /*0b11,*/
			T::MODER::FieldValues::Input::Value,
			static_cast<ModerType>(pinNum * uint8_t{2U}));
		volatile auto value = T::MODER::Get();
		value &= ~(3 << (pinNum * 2U));
		value |= (T::MODER::FieldValues::Input::Value << (pinNum * 2U));
		T::MODER::Write(value);
	}

	static void SetOutput(std::uint32_t pinNum)
	{
		assert(pinNum <= 15U);
		ModerType::Set(
			T::MODER::Address,
			T::MODER::FieldValues::Output::Mask,
			T::MODER::FieldValues::Output::Value,
			static_cast<ModerType>(pinNum * uint8_t{2U}));
	}

	static void SetAlternate(std::uint32_t pinNum)
	{
		assert(pinNum <= 15U);
		ModerType::Set(
			T::MODER::Address,
			T::MODER::FieldValues::Alternate::Mask,
			T::MODER::FieldValues::Alternate::Value,
			static_cast<ModerType>(pinNum * uint8_t{2U}));
	}
};

template<typename... T>
struct Pins {

	static void Toggle()
	{
		(T::Toggle() | ...);
	}

	static void Set()
	{
		(T::Set() | ...);
	}

	static void Reset()
	{
		(T::Reset() | ...);
	}

	static void SetOutput()
	{
		(T::SetOutput() | ...);
	}

	static void SetInput()
	{
		(T::SetInput() | ...);
	}

	static void SetAnalog()
	{
		(T::SetInput() | ...);
	}

	static void SetAlternate()
	{
		(T::SetInput() | ...);
	}
};

#endif // REGISTERS_PORT_HPP_
