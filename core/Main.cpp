#include "Main.h"

#include "rccregisters.hpp"   // For RCC
#include "gpiocregisters.hpp" // For Gpioc
#include "gpioaregisters.hpp" // For Gpioa
#include "port.hpp"
#include "pin.hpp"

void SystemInit(void){
	RCC::CR::HSEON::Value1::Set();
    RCC::CFGR::SW::Value1::Set();

    RCC::AHBENRPack<RCC::AHBENR::IOPAEN::Value1, RCC::AHBENR::IOPCEN::Value1>::Set();

    GPIOA::MODER::MODER0::Value0::Set(); // Input Mode
    GPIOA::PUPDR::PUPDR0::Value1::Set(); // Value 1 - Pull-up
}

int main(){

    GPIOC::MODERPack<
        GPIOC::MODER::MODER6::Value1,
        GPIOC::MODER::MODER7::Value1,
        GPIOC::MODER::MODER8::Value1,
        GPIOC::MODER::MODER9::Value1>::Set(); // Value 1 - Output mode

    GPIOC::OSPEEDRPack<
        GPIOC::OSPEEDR::OSPEEDR6::Value0,
        GPIOC::OSPEEDR::OSPEEDR7::Value0,
        GPIOC::OSPEEDR::OSPEEDR8::Value0,
        GPIOC::OSPEEDR::OSPEEDR9::Value0>::Set(); // Value 0 - LowSpeed

    GPIOC::BSRRPack<
        GPIOC::BSRR::BS7::Value1,
        GPIOC::BSRR::BS8::Value1,
        GPIOC::BSRR::BS9::Value1>::Write(); // Value 0 - LowSpeed

    // GPIOA::IDR::IDR0::Write(1);

    using Button = Pin<Port<GPIOA>, 0U, PinReadable>;
    using RedLed = Pin<Port<GPIOC>, 6U, PinWriteable>;

    while (1)
    {
        if (Button::Get()){
            // GPIOC::BSRR::BS6::Value1::Write();
            RedLed::Set();
        } else {
            // GPIOC::BRR::BR6::Value1::Write();
            RedLed::Reset();
        }
    }
}
