#ifndef REGISTERS_REGISTERBASE_HPP_
#define REGISTERS_REGISTERBASE_HPP_

#include "registertype.hpp"
#include "accessmode.hpp"

//Базовый класс для работы с регистром
template<std::uintptr_t address, size_t size, Accessable AccessMode>
struct RegisterBase {
	static constexpr auto Address = address;
	using Type = RegisterType<size>::Type;

	//Метод Write будет работать только для регистров, в которые можно записать значение
	template<Writable T = AccessMode>
	static void Write(Type value)
	{
		*reinterpret_cast<volatile Type *>(address) = value;
	}

	//Метод Write будет работать только для регистров, в которые можно записать значение
	template<Type value, Writable T = AccessMode>
	static void Write()
	{
		*reinterpret_cast<volatile Type *>(address) = value;
	}

	//Метод Write будет работать только для регистров, в которые можно записать значение
	template<Writable T = AccessMode>
	static void Set(Type value)
	{
		Type oldRegValue = *reinterpret_cast<volatile Type *>(address); //Сохраняем текущее значение регистра
		*reinterpret_cast<volatile Type *>(address) = static_cast<Type>(oldRegValue | value);
	}

	//Метод Write будет работать только для регистров, в которые можно записать значение
	template<Writable T = AccessMode>
	static void Toggle(Type value)
	{
		*reinterpret_cast<volatile Type *>(address) ^= value;
	}

	//Метод Get возвращает целое значение регистра, будет работать только для регистров, которые можно считать
	template<Readable T = AccessMode>
	static Type Get()
	{
		return *reinterpret_cast<volatile Type *>(address);
	}
};

#endif // REGISTERS_REGISTERBASE_HPP_