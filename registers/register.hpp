#ifndef REGISTERS_REGISTER_HPP_
#define REGISTERS_REGISTER_HPP_

#include "fieldvalue.hpp"
#include "registertype.hpp"
#include "accessmode.hpp"

//Класс для работы с регистром, можно передавать список Битовых полей для установки и проверки
template<std::uintptr_t address, size_t size, Accessable AccessMode, typename FieldValueBaseType, typename ...Args>
class Register
{
 public:
    static_assert(!IsIdentType<Args...>::value, "There is the same field value type");

    using Type = RegisterType<size>::Type;
    //Метод Set устанавливает битовые поля, только если регистр может использоваться для записи
    template<Writable T = AccessMode>
    static void Set()
    {
        Type newRegValue = *reinterpret_cast<volatile Type*>(address); //Сохраняем текущее значение регистра
        static constexpr auto mask = GetMask();
        static constexpr auto value = GetValue();

        newRegValue &= ~mask; //Сбрасываем битовые поля, которые нужно будет установить
        newRegValue |= value; //Устанавливаем новые значения битовых полей
        *reinterpret_cast<volatile Type*>(address) = newRegValue; //Записываем в регистра новое значение
    }

    //Метод Write устанавливает битовые поля, только если регистр может использоваться для записи
    template<Writable T = AccessMode>
    static void Write()
    {
        static constexpr auto value = GetValue();
        *reinterpret_cast<volatile Type*>(address) = value; //Записываем в регистра новое значение
    }

    //Метод IsSet проверяет что все битовые поля из переданного набора установлены
    template<Readable T = AccessMode>
    static bool IsSet()
    {
        static constexpr auto mask = GetMask();
        static constexpr auto value = GetValue();
        Type newRegValue = *reinterpret_cast<volatile Type*>(address);

        return ((newRegValue & mask) == value);
    }

 private:
    //Вспомогательный метод, возвращает маску для конкретного битового поля на этапе компиляции.
    //Метод определен только в случае, если тип битового поля и базовый тип битового поля для регистра совпадают.
    //Т.е. нельзя устанвоить набор битов не соотвествующих набору для для данного регистра.
    template<typename T>
    requires std::same_as<typename T::BaseType, FieldValueBaseType>
    static constexpr auto GetIndividualMask()
    {
        constexpr Type result = T::Mask << T::Offset;
        return result;
    }

    //Вспомогательный метод, расчитывает общую маску для всего набора битовых полей на этапе компиляции.
    static constexpr auto GetMask()
    {
        return (GetIndividualMask<Args>() | ...);//result ;
    }

    //Вспомогательный метод, возвращает значение для конктретного битового поля на этапе компиляции.
    //Метод определен только в случае, если тип битового поля и базовый тип битового поля для регистра совпадают.
    //Т.е. нельзя устанвоить набор битов не соотвествующих набору для для данного регистра.
    template<typename T>
    requires std::same_as<typename T::BaseType, FieldValueBaseType>
    static constexpr auto GetIndividualValue()
    {
        constexpr Type result = T::Value << T::Offset;
        return result;
    }

    //Вспомогательный метод, расчитывает значение которое нужно установить в регистре для всего набора битовых полей
    static constexpr auto GetValue()
    {
        return (GetIndividualValue<Args>() | ...);
    }
};

#endif // REGISTERS_REGISTER_HPP_

