#ifndef REGISTERS_ACCESSMODE_HPP_
#define REGISTERS_ACCESSMODE_HPP_

#include <concepts>

struct AccessBase {};
struct NoAccess : public AccessBase {};
struct WriteMode : public AccessBase {};
struct ReadMode : public AccessBase {};
struct ReadWriteMode : public AccessBase {};

template<typename T>
concept Accessable = std::derived_from<T, AccessBase>;

template<typename T>
concept Writable = std::same_as<T, WriteMode> || std::same_as<T, ReadWriteMode>;

template<typename T>
concept Readable = std::same_as<T, ReadMode> || std::same_as<T, ReadWriteMode>;

#endif // REGISTERS_ACCESSMODE_HPP_
