#ifndef REGISTERS_REGISTERFIELD_HPP_
#define REGISTERS_REGISTERFIELD_HPP_

#include "accessmode.hpp"
#include "auxiliary.hpp"
#include <limits>
#include <concepts>

template<typename T>
concept Registers = std::unsigned_integral<typename T::Type>;

//Базовый класс для работы с битовыми полями регистров
template<Registers Reg, size_t offset, size_t size, Accessable AccessMode>
struct RegisterField {
	using RegType = Reg::Type;
	using Register = Reg;

	static constexpr RegType Offset = offset;
	static constexpr RegType Size = size;
	static constexpr RegType Mask = (size < sizeof(RegType) * 8U) ? (static_cast<RegType>(
										(static_cast<RegType>(1U) << size) - static_cast<RegType>(1U))) :
																	std::numeric_limits<RegType>::max();
	using Access = AccessMode;

	//Метод устанавливает значение битового поля, только в случае, если оно доступно для записи
	template<Writable T = AccessMode>
	static void Set(RegType value)
	{
		assert((size < sizeof(RegType) * 8U) ? (value <= ((static_cast<RegType>(1U) << size) - static_cast<RegType>(1U))) :
											   (value <= std::numeric_limits<RegType>::max()));

		RegType newRegValue = *reinterpret_cast<volatile RegType *>(Reg::Address); //Сохраняем текущее значение регистра

		newRegValue &= ~(Mask << offset); //Вначале нужно очистить старое значение битового поля
		newRegValue |= (value << offset); // Затем установить новое

		*reinterpret_cast<volatile RegType *>(Reg::Address) = newRegValue; //И записать новое значение в регистр
	}

	//Метод устанавливает значение битового поля, только в случае, если оно достпуно для записи
	template<RegType value, Writable T = AccessMode>
	static void Set()
	{
		static_assert((size < sizeof(RegType) * 8U) ? (value <= ((static_cast<RegType>(1U) << size) - static_cast<RegType>(1U))) :
													  (value <= std::numeric_limits<RegType>::max()),
			"Value type size is more then the field size");
		RegType newRegValue = *reinterpret_cast<volatile RegType *>(Reg::Address); //Сохраняем текущее значение регистра

		newRegValue &= ~(Mask << offset); //Вначале нужно очистить старое значение битового поля
		newRegValue |= (value << offset); // Затем установить новое

		*reinterpret_cast<volatile RegType *>(Reg::Address) = newRegValue; //И записать новое значение в регистр
	}

	//Метод устанавливает значение битового поля, только в случае, если оно достпуно для записи
	template<Writable T = AccessMode>
	static void Write(RegType value)
	{
		assert((size < sizeof(RegType) * 8U) ? (value <= ((static_cast<RegType>(1U) << size) - static_cast<RegType>(1U))) :
											   (value <= std::numeric_limits<RegType>::max()));
		*reinterpret_cast<volatile RegType *>(Reg::Address) = (value << offset);
	}

	//Метод устанавливает значение битового поля, только в случае, если оно достпуно для записи
	template<RegType value, Writable T = AccessMode>
	static void Write()
	{
		static_assert((size < sizeof(RegType) * 8U) ? (value <= ((static_cast<RegType>(1U) << size) - static_cast<RegType>(1U))) :
													  (value <= std::numeric_limits<RegType>::max()),
			"Value type size is more then the field size");
		*reinterpret_cast<volatile RegType *>(Reg::Address) = (value << offset);
	}

	//Метод устанавливает проверяет установлено ли значение битового поля
	template<Readable T = AccessMode>
	static RegType Get()
	{
		return ((*reinterpret_cast<volatile RegType *>(Reg::Address)) & (Mask << offset)) >> offset;
	}
};

#endif // REGISTERS_REGISTERFIELD_HPP_
