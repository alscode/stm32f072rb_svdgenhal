#ifndef REGISTERS_FIELDVALUEBASE_HPP_
#define REGISTERS_FIELDVALUEBASE_HPP_

#include "accessmode.hpp"
#include "registerfield.hpp"

//Базовый класс для работы с битовыми полями регистров
template<typename Field, typename Base, Field::Register::Type value>
struct FieldValueBase {
	using RegType = Field::Register::Type;

	//Метод устанавливает значение битового поля, только в случае, если оно достпуно для записи
	template<Writable T = Field::Access>
	static void Set()
	{
		RegType newRegValue = *reinterpret_cast<volatile RegType *>(Field::Register::Address); //Сохраняем текущее значение регистра

		newRegValue &= ~(Field::Mask << Field::Offset); //Вначале нужно очистить старое значение битового поля
		newRegValue |= (value << Field::Offset); // Затем установить новое

		*reinterpret_cast<volatile RegType *>(Field::Register::Address) = newRegValue; //И записать новое значение в регистр
	}

	//Метод устанавливает значение битового поля, только в случае, если оно достпуно для записи
	template<Writable T = Field::Access>
	static void Write()
	{
		*reinterpret_cast<volatile RegType *>(Field::Register::Address) = static_cast<RegType>(value << Field::Offset);
	}

	//Метод проверяет установлено ли значение битового поля
	template<Readable T = Field::Access>
	static bool IsSet()
	{
		return ((*reinterpret_cast<volatile RegType *>(Field::Register::Address)) & static_cast<RegType>(Field::Mask << Field::Offset)) == value << Field::Offset;
	}
};

#endif // REGISTERS_FIELDVALUEBASE_HPP_
