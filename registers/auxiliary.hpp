#ifndef REGISTERS_AUXILIARY_HPP_
#define REGISTERS_AUXILIARY_HPP_

#include <algorithm>
#include <type_traits>

template<typename... args>
struct Attributes {
private:
	template<typename TAttr, typename T = void, typename... arg>
	struct Attr {
		using Type = std::conditional_t<std::is_same_v<TAttr, T>, T, typename Attr<TAttr, arg...>::Type>;
	};

	template<typename TAttr>
	struct Attr<TAttr, void> {
		using Type = void;
	};

public:
	template<typename T>
	using Attribute = typename Attr<T, args...>::Type;

	template<typename T>
	static constexpr bool HasAttribute()
	{
		return !std::is_same<Attribute<T>, void>::value;
	}
};

template<typename T, typename... Ts>
consteval bool IsIdent()
{
	if constexpr (sizeof...(Ts) != 0U) {
		if (Attributes<typename Ts::FieldType...>::template HasAttribute<typename T::FieldType>()) {
			return true;
		} else {
			return IsIdent<Ts...>();
		}
	} else {
		return false;
	}
	assert(false);
	return false;
}

template<typename... T>
struct IsIdentType {
	static constexpr bool value = IsIdent<T...>();
};

#endif // REGISTERS_AUXILIARY_HPP_
