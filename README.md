# STM32F072RB_SvdGenHal

1. git clone https://gitlab.com/alscode/stm32f072rb_svdgenhal.git SvdGenHal
2. cd SvdGenHal
3. python3 reg_wrappers_generator.py STM32F0x2.svd
4. mkdir -p build && cd build
5. cmake -DCMAKE_BUILD_TYPE=Release ..
6. cmake --build .